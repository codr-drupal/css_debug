# CSS Debug

## Thank you !

Huge thanks to [Emeric](https://www.emoveo.design/) for the original code.

## How to require it

Add the package registry in your composer.json

```json
    "repositories": {
        "codr-drupal": {
            "type": "composer",
            "url": "https://gitlab.com/api/v4/group/codr-drupal/-/packages/composer/packages.json"
        }
    }
```

Then require the profile

```sh
composer require codr-drupal/css_debug
```

## Use current dev branch

Add the gitlab repository in your composer.json

```json
    "repositories": {
        "codr-drupal/css_debug": {
            "type": "gitlab",
            "url": "https://gitlab.com/codr-drupal/css_debug.git"
        }
    }
```

Keep in mind you also must keep the package registry (previous chapter) because [Composer can't load repositories recursively](https://getcomposer.org/doc/faqs/why-cant-composer-load-repositories-recursively.md)

Then require the profile

```sh
composer require codr-drupal/css_debug:10.x-dev
```