(function (Drupal, once) {
  "use strict";

  Drupal.behaviors.css_debug = {
    attach(_, __) {
      once("css_debug", "body").forEach(element => {

        const debugToggle = document.createElement("div");
        debugToggle.setAttribute("id", "debug-wrapper");
        debugToggle.classList.add("debug-wrapper");
        debugToggle.innerHTML =
          '<p>Debug mode</p><label class="toggle"><input type="checkbox"/><span class="slider"></span></label>';
        element.appendChild(debugToggle);

        debugToggle.addEventListener("click", () => {
          debugToggle.querySelector("input").checked
            ? element.classList.add("css_debug")
            : element.classList.remove("css_debug");
        });
      });
    },
  };
})(Drupal, once);
